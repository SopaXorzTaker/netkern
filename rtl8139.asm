; RTL8139 driver

; Registers
RTL8139_CMD  equ 0x37
RTL8139_CAPR equ 0x38
RTL8139_IMR  equ 0x3c
RTL8139_ISR  equ 0x3e
RTL8139_RCR  equ 0x44

; Flags
RTL8139_ROK equ 1
RTL8139_TOK equ 1<<2
RTL8139_TE  equ 1<<2
RTL8139_RE  equ 1<<3

RTL8139_AAP  equ 1
RTL8139_APM  equ 1<<1
RTL8139_AM   equ 1<<2
RTL8139_AB   equ 1<<3
RTL8139_WRAP equ 1<<7

RTL8139_BAR  equ 1<<13
RTL8139_PAM  equ 1<<14
RTL8139_MAR  equ 1<<15

; Ethernet
ETHERNET_INT_RX_BUFFER  equ 0x0000
ETHERNET_INT_RX_SEGMENT equ 0x6000
ETHERNET_TX_BUFFER      equ 0x25ec
ETHERNET_TX_SEGMENT     equ 0x6000
ETHERNET_RX_BUFFER      equ 0x2bc8
ETHERNET_RX_SEGMENT     equ 0x6000

; Poll status
rtl8139.rxPoll db 0

; The driver context
rtl8139._context:
    .baseAddress    dw 0
    .irq            db 0
    .transmitStart  dw 0
    .transmitStatus dw 0
    .readPtrOffset  dw 0

rtl8139.mac times 6 db 0

; PCI config location
rtl8139._pciConfig:
    .bus        db 0
    .device     db 0
    .function   db 0
    .register   db 0

; Initializes the RTL8139
rtl8139.init:
    push eax
    push ebx
    push ecx
    push edx

    xor cx, cx

    ; BX will point to our PCI config structure
    mov bx, rtl8139._pciConfig

    ; Function 0, register 0
    mov byte [rtl8139._pciConfig.function], 0
    mov byte [rtl8139._pciConfig.register], 0

    .checkDevice:
        ; Read the configuration for the device at that address
        mov [rtl8139._pciConfig.bus],    ch
        mov [rtl8139._pciConfig.device], cl
        call pci.configRead

        ; Test VID/PID
        cmp eax, 0x813910ec

        ; Either the device doesn't exist (VID=0xffff) or it's not an RTL8139
        jne .nextDevice

        ; It's the right device so keep the PCI config location and read more registers
        mov si, rtl8139.foundMessage
        call console.print

        mov ax, cx
        call console.printHex
        call console.newline

        jmp .found

    .nextDevice:
        inc cl
        cmp cl, 32
        jz .nextBus
        jmp .checkDevice
    .nextBus:
        xor cl, cl
        inc ch
        jz .notFound
        jmp .checkDevice

    .notFound:
        mov si, rtl8139.notFoundMessage
        call console.print
        jmp kernel.halt

    .addressError:
        mov si, rtl8139.addressErrorMessage
        call console.print
        jmp kernel.halt

    .found:
        ; Read BAR0
        mov byte [rtl8139._pciConfig.register], 0x04
        call pci.configRead

        ; Test the lower bit
        test eax, 0x1
        jz .addressError

        ; Calculate the I/O space address
        and ax, 0xfffc

        ; Store it
        mov [rtl8139._context.baseAddress], ax

        ; Print the base address message
        mov si, rtl8139.baseAddressMessage
        call console.print

        ; Print AX (our base address)
        call console.printHex

        ; Newline
        call console.newline

        ; Read the IRQ number
        mov byte [rtl8139._pciConfig.register], 0x0f
        call pci.configRead
        mov [rtl8139._context.irq], al

        ; Unmask the IRQ
        call pic8259.unmask

        ; Register the interrupt vector
        xor bx, bx
        mov bl, al

        ; Compute the interrupt number
        cmp bl, 7
        jna .skip_int_offset
        add bl, 0x68 ; 0x70 - 8
    .skip_int_offset:

        ; Print the IRQ message
        mov si, rtl8139.irqMessage
        call console.print

        ; Print the IRQ as a hex number
        mov dh, bl
        call console.printHexByte

        ; Newline
        call console.newline

        ; Multiply BX by 4 to yield the index into the vector table
        shl bx, 2

        ; Store the value at 0x0000:BX, the IVT
        cli
        push es
        xor ax, ax
        mov es, ax

        ; Write the address of our interrupt handler
        mov word [es:bx], rtl8139._interrupt
        mov word [es:bx+2], KERNEL_SEGMENT
        pop es

        ; Enable bus mastering and interrupts for the RTL8139
        mov bx, rtl8139._pciConfig
        mov byte [rtl8139._pciConfig.register], 0x01
        call pci.configRead

        ; Allow bus mastering
        or eax, 1<<2

        ; Interrupt Disable = 0
        and eax, ~(1<<10)

        ; Apply
        call pci.configWrite

        ; Power up the RTL8139
        mov dx, [rtl8139._context.baseAddress]
        add dx, 0x52
        mov al, 0x00
        out dx, al

        ; Software reset
        mov dx, [rtl8139._context.baseAddress]
        add dx, RTL8139_CMD
        mov al, 0x10
        out dx, al

    .wait_reset:
        in al, dx
        test al, 0x10
        jnz .wait_reset

        ; Initialize the receive buffer
        mov dx, [rtl8139._context.baseAddress]
        add dx, 0x30
        mov eax, ETHERNET_INT_RX_BUFFER+ETHERNET_INT_RX_SEGMENT*16
        out dx, eax

        ; Set IMR (TOK and ROK)
        mov dx, [rtl8139._context.baseAddress]
        add dx, RTL8139_IMR
        mov ax, RTL8139_TOK | RTL8139_ROK
        out dx, ax

        ; Configure the receive buffer (accept all packets)
        mov dx, [rtl8139._context.baseAddress]
        add dx, RTL8139_RCR
        mov eax, RTL8139_AB | RTL8139_AM | RTL8139_APM | RTL8139_WRAP
        out dx, eax

        ; Enable reception and transmitting
        mov dx, [rtl8139._context.baseAddress]
        add dx, RTL8139_CMD
        mov al, RTL8139_RE | RTL8139_TE
        out dx, al ; Sets the RE and TE bits high

        ; Re-enable the interrupts
        sti

        ; Reset the transmit start/status registers
        mov word [rtl8139._context.transmitStart],  0x20
        mov word [rtl8139._context.transmitStatus], 0x10

        ; Reset readPtrOffset
        mov word [rtl8139._context.readPtrOffset], 0

        ; Reset the poll status
        mov byte [rtl8139.rxPoll], 0

        ; Read the MAC
        mov dx, [rtl8139._context.baseAddress]
        in eax, dx

        ; Store the first part
        mov [rtl8139.mac], al
        shr eax, 8
        mov [rtl8139.mac+1], al
        shr eax, 8
        mov [rtl8139.mac+2], al
        shr eax, 8
        mov [rtl8139.mac+3], al

        ; Read the next two bytes
        add dx, 4
        in ax, dx
        mov [rtl8139.mac+4], al
        shr ax, 8
        mov [rtl8139.mac+5], al

        ; Print the init message
        mov si, rtl8139.initMessage
        call console.print

        ; Print the MAC address
        mov bx, rtl8139.mac
        call net.printMAC

        ; Newline
        call console.newline

        pop edx
        pop ecx
        pop ebx
        pop eax
        ret

; Interrupt handler (internal)
rtl8139._interrupt:
    ; Save the registers
    pushad
    pushfd

    ; Preserve DS, ES, FS
    push ds
    push es
    push fs

    ; Make the DS point into our kernel
    mov ax, KERNEL_SEGMENT
    mov ds, ax

    ; Read the status
    mov dx, [rtl8139._context.baseAddress]
    add dx, RTL8139_ISR
    in ax, dx

    mov si, rtl8139.interruptMessage
    call console.print

    ; Print AX (the status)
    call console.printHex

    ; Newline
    call console.newline

    ; Received data?
    test ax, RTL8139_ROK
    jz .noRx

    ; Update the poll status
    mov byte [rtl8139.rxPoll], 1

    ; Prepare to copy the packet
    mov ax, ETHERNET_INT_RX_SEGMENT
    mov es, ax
    mov si, ETHERNET_INT_RX_BUFFER
    add si, [rtl8139._context.readPtrOffset]

    ; Get packet length
    mov eax, [es:si]
    shr eax, 16

    mov cx, ax
    mov dx, ax

    ; Copy the packet over to the actual RX buffer
    mov ax, ETHERNET_RX_SEGMENT
    mov fs, ax
    mov di, ETHERNET_RX_BUFFER

.copyByte:
    mov al, [es:si]
    mov [fs:di], al
    inc si
    inc di

    dec cx
    jnz .copyByte

    ; Increment the read offset
    add word [rtl8139._context.readPtrOffset], dx
    add word [rtl8139._context.readPtrOffset], 4

    ; Round up to the nearest multiple of four
    add word [rtl8139._context.readPtrOffset], 3
    and word [rtl8139._context.readPtrOffset], ~3

    mov ax, [rtl8139._context.readPtrOffset]
    sub ax, 0x10

    ; Wrap the read offset
    cmp word [rtl8139._context.readPtrOffset], 8192+1500+16
    jb .skipOverflow
    sub word [rtl8139._context.readPtrOffset], 8192+1500+16
.skipOverflow:
    ; Write
    mov dx, [rtl8139._context.baseAddress]
    add dx, RTL8139_CAPR
    out dx, ax

.noRx:
    ; Acknowledge TX/RX
    mov dx, [rtl8139._context.baseAddress]
    add dx, RTL8139_ISR
    in ax, dx
    out dx, ax

    ; Signal EOI
    mov al, [rtl8139._context.irq]
    call pic8259.eoi

    ; Restore the registers
    pop fs
    pop es
    pop ds
    popfd
    popad
    iret

; Send packet at ETHERNET_TX_BUFFER
; CX is the data size
rtl8139.sendPacket:
    push eax
    push edx
    mov dx, [rtl8139._context.baseAddress]
    add dx, word [rtl8139._context.transmitStart]

    ; Mask the interrupt
    call rtl8139.maskInterrupt

    ; Write the transmit start
    mov eax, ETHERNET_TX_BUFFER+ETHERNET_TX_SEGMENT*16
    out dx, eax

    ; Write the transmit flags
    mov eax, ecx
    and eax, 0xfff

    mov dx, [rtl8139._context.baseAddress]
    add dx, word [rtl8139._context.transmitStatus]
    out dx, eax

    ; Switch to the next register pair
    add word [rtl8139._context.transmitStart], 4
    add word [rtl8139._context.transmitStatus], 4

    ; Test for roll-over
    cmp word [rtl8139._context.transmitStart], 0x30
    jne .skip_rollover

    ; Reset the registers
    mov word [rtl8139._context.transmitStart],  0x20
    mov word [rtl8139._context.transmitStatus], 0x10

.skip_rollover:
    ; The packet was sent
    mov si, rtl8139.packetMessage
    call console.print

    pop edx
    pop eax
    ret

; Wait for a packet to arrive (or a timeout to occur)
rtl8139.wait:
    push ax
    push cx
    push si

    ; Log
    mov si, rtl8139.waitMessage
    call console.print

    ; Clear the poll flag
    mov byte [rtl8139.rxPoll], 0

    ; Clear the carry
    clc

    ; Unmask the interrupt
    call rtl8139.unmaskInterrupt

    ; Store the current time
    mov cx, [pit.state.ticks]

    ; Wait until the poll indicator is set (or a timeout occurs)
.wait:
    cmp byte [rtl8139.rxPoll], 1
    jz .done

    ; Wait for an interrupt
    hlt

    ; Timeout?
    mov ax, [pit.state.ticks]
    sub ax, cx

    cmp ax, 180
    jb .wait

    mov si, rtl8139.timeoutMessage
    call console.print

    ; Timeout (carry set)
    stc

    ; Done
.done:
    jc .skipWaitDone
    mov si, rtl8139.waitDoneMessage
    call console.print
.skipWaitDone:

    ; Restore the registers
    pop si
    pop cx
    pop ax
    ret

; Mask the RTL8139 interrupt
rtl8139.maskInterrupt:
    push ax
    mov al, [rtl8139._context.irq]
    call pic8259.mask
    pop ax
    ret

; Unmask the RTL8139 interrupt
rtl8139.unmaskInterrupt:
    push ax
    mov al, [rtl8139._context.irq]
    call pic8259.unmask
    pop ax
    ret


rtl8139.foundMessage db "[+] rtl8139: found the device at ", 0
rtl8139.notFoundMessage db "[!] rtl8139: no devices found.", 13, 10, 0
rtl8139.addressErrorMessage db "[!] rtl8139: the base address is not in the I/O area.", 13, 10, 0
rtl8139.baseAddressMessage db "[+] rtl8139: the I/O base address is 0x", 0
rtl8139.irqMessage db "[+] rtl8139: the interrupt number is 0x", 0
rtl8139.interruptMessage db "[*] rtl8139: interrupt! Status: 0x", 0
rtl8139.initMessage db "[+] rtl8139: initialization finished, MAC: ", 0
rtl8139.packetMessage db "[*] rtl8139: packet sent!", 13, 10, 0
rtl8139.waitMessage db "[*] rtl8139: waiting for a packet...", 13, 10, 0
rtl8139.waitDoneMessage db "[*] rtl8139: done waiting!", 13, 10, 0
rtl8139.timeoutMessage db "[*] rtl8139: timeout!", 13, 10, 0
