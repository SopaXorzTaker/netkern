KERNEL_SEGMENT equ 0x07c0

; This is an El-Torito boot image
%include "eltorito.asm"

; Include the routines
%include "console.asm"
%include "pic8259.asm"
%include "pit.asm"
%include "pci.asm"
%include "rtl8139.asm"
%include "net.asm"

kernel.entryPoint:
    ; Store the drive number (if we ever want to read the CD)
    mov [kernel.bootStatus.driveNumber], dl
    xor cl, cl

    ; Kernel loaded
    mov si, kernel.bootMessage
    call console.print

    ; Initialize the PIT
    call pit.init

    ; Initialize the networking
    call net.init

    ; Test
    mov eax, 0x08080808
    mov si, dns
    mov bx, 53
    mov dx, 12345
    mov cx, dns.length
    call net.udp.init
    call net.udp.sendPacket
    call net.ip.recvPacket

    mov si, dumpMessage
    call console.print

    mov ax, cx
    call console.printHex

.printByte:
    test cx, cx
    jz kernel.halt

    mov dh, [es:di]
    call console.printHexByte

    inc di
    dec cx
    jmp .printByte

kernel.halt:
    hlt
    jmp kernel.halt

kernel.bootStatus:
    .driveNumber db 0

kernel.bootMessage db "[+] kernel: kernel loaded", 13, 10, 0


dns:
    .id dw 0xabcd
    .flags dw 0
    .qdcount db 0, 1
    .ancount db 0, 0
    .nscount db 0, 0
    .arcount db 0, 0

    .qname db 3,"www",6,"google",3,"com",0
    .qtype db 0x00, 0x01
    .qclass db 0x00, 0x01
    .length equ $-dns

dumpMessage db "[*] kernel: dumping UDP packet: ", 0
