all: clean kernel.bin netkern.iso test

clean:
	rm -f kernel.bin netkern.iso

kernel.bin:
	nasm kernel.asm -o kernel.bin

netkern.iso:
	genisoimage -no-emul-boot -b kernel.bin -o netkern.iso kernel.bin

test:
	qemu-system-i386 -cdrom netkern.iso -netdev user,id=user.0 -device rtl8139,netdev=user.0 -object filter-dump,id=file.1,netdev=user.0,file=traffic.dat

