; Programmable interval timer routines
PIT_DATA      equ 0x40
PIT_COMMAND   equ 0x43
PIT_INTERRUPT equ 0x08

; PIT state
pit.state:
    .ticks dw 0


; Set an interrupt
pit.init:
    push ax
    push bx
    push es
    pushfd

    ; Write our interrupt vector
    cli
    xor ax, ax
    mov es, ax
    mov bx, PIT_INTERRUPT*4
    mov word [es:bx], pit._interrupt
    mov word [es:bx+2], KERNEL_SEGMENT

    ; Clear the tick counter
    mov word [pit.state.ticks], 0

    popfd
    pop es
    pop bx
    pop ax
    ret

pit._interrupt:
    pushad
    pushfd

    ; Make DS point into our kernel
    mov ax, KERNEL_SEGMENT
    mov ds, ax

    ; Increment the tick counter
    inc word [pit.state.ticks]

    ; Signal EOI
    mov al, 0
    call pic8259.eoi

    popfd
    popad
    iret
