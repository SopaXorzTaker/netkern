; PCI routines.

; The device location structure is 4 bytes long, as follows:
;  bus, device, function, register.

; Set the PCI configuration address according to
;  the device location structure pointed to by BX.
pci._configSetAddress:
    push eax
    push dx

    ; Zero out EAX
    xor eax, eax

    ; Bus
    or al, [bx]

    ; Device
    shl eax, 5
    or al, [bx+1]

    ; Function
    shl eax, 3
    or al, [bx+2]

    ; Register
    shl eax, 6
    or al, [bx+3]

    ; Zeros
    shl eax, 2

    ; Enable
    or eax, 1<<31

    ; Write the address
    mov dx, 0xcf8
    out dx, eax

    pop dx
    pop eax
    ret

; Reads a dword of the PCI configuration space.
; BX points to the PCI device location structure.
; EAX will contain the word we have read.
pci.configRead:
    ; Set the access address
    call pci._configSetAddress

    ; Preserve DX
    push dx

    ; Read the configuration dword
    mov dx, 0xcfc
    in eax, dx

    ; Restore DX
    pop dx
    ret

; Writes a dword of the PCI configuration space.
; BX points to the PCI device location structure.
; EAX contains the word we want to write.
pci.configWrite:
    ; Set the access address
    call pci._configSetAddress

    ; Preserve DX
    push dx

    ; Write the configuration dword
    mov dx, 0xcfc
    out dx, eax

    ; Restore DX
    pop dx
    ret
