; Various console functions.

; Prints a string at DS:SI.
console.print:
    ; Preserve the registers
    push ax
    push si

    ; Clear the direction flag, just in case
    cld

    ; int 10h, ah = 0x0e - output a character
    mov ah, 0x0e

    .loop:
        ; Load string byte
        lodsb

        ; If zero, then return
        test al, al
        jz .end

        ; Interrupt
        int 10h
        jmp .loop

    .end:
        ; Restore the registers
        pop si
        pop ax
        ret


; Prints the hexadecimal value of DH
console.printHexByte:
    push ax
    push bx

    ; Separate the hex digits
    mov bh, dh
    mov bl, dh
    shr bh, 4
    and bl, 0x0f

    ; ASCII-ify the digits
    add bh, 0x30
    cmp bh, 0x39
    jna .no_adjust_high
    add bh, 0x07
.no_adjust_high:
    add bl, 0x30
    cmp bl, 0x39
    jna .no_adjust_low
    add bl, 0x07
.no_adjust_low:

    ; Print the digits
    mov ah, 0x0e
    mov al, bh
    int 10h

    mov al, bl
    int 10h

    pop bx
    pop ax
    ret

; Prints the hexadecimal value of AX
console.printHex:
    push dx
    mov dx, ax

    ; Upper byte
    call console.printHexByte

    ; Lower byte
    shl dx, 8
    call console.printHexByte

    pop dx
    ret

; Outputs a character in AL.
console.putchar:
    push ax
    mov ah, 0x0e
    int 10h
    pop ax
    ret

; Outputs a newline.
console.newline:
    push ax
    mov al, 0x0d
    call console.putchar

    mov al, 0x0a
    call console.putchar
    pop ax
    ret
